import { lazy } from 'react';

// Not Lazy Load (10.73s, 190 kB)
// import Counter from '../components/Counter';
// import CreateNewPlayer from '../components/CreateNewPlayer';
// import RatingComponent from '../components/RatingComponent'
// import Suit from '../components/Suit';

// Lazy Loaded (12.63s, 190 kB)

const CreateNewPlayer = lazy(() =>
    import ('../components/CreateNewPlayer'));

const routes = [

    {
        path: '/',
        component: < CreateNewPlayer / > ,
        exact: true,
    },
    {
        path: '*',
        component: "Not Found",
        exact: false,
    },
]

export default routes;